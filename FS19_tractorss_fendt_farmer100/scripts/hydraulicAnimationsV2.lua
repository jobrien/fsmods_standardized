-- by modelleicher
-- www.schwabenmodding.bplaced.net

hydraulicAnimations = {};

function hydraulicAnimations.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(Motorized, specializations);
end;

function hydraulicAnimations.registerEventListeners(vehicleType)
	--print("registerEventListeners called");
	SpecializationUtil.registerEventListener(vehicleType, "onLoad", hydraulicAnimations);
	SpecializationUtil.registerEventListener(vehicleType, "onUpdate", hydraulicAnimations);
end;

function hydraulicAnimations:onLoad(savegame)
    self.hh = {};
    self.hh.arm = I3DUtil.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.hydraulicAnimation.backArm#node"), self.i3dMappings);
    self.hh.armGetRot = I3DUtil.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.hydraulicAnimation.backArm#getRotRef"), self.i3dMappings);
    self.hh.armLengthMulti = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.hydraulicAnimation.backArm#lengthMultiplicator"), 1);
    self.hh.armAddDeg = Utils.getNoNil(getXMLFloat(self.xmlFile, "vehicle.hydraulicAnimation.backArm#addDegrees"), 0);
    
    self.hh.streben = {};
	

    
    for i=1, 2 do
        local t = {};
        t.index = I3DUtil.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.hydraulicAnimation.streben"..i.."#node"), self.i3dMappings);
        t.ref = I3DUtil.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.hydraulicAnimation.streben"..i.."#ref"), self.i3dMappings);
        t.scaleRef = I3DUtil.indexToObject(self.components, getXMLString(self.xmlFile, "vehicle.hydraulicAnimation.streben"..i.."#scaleRef"), self.i3dMappings);
        if t.index ~= "" and t.index ~= nil then
            table.insert(self.hh.streben, t)
            ax, ay, az = getWorldTranslation(self.hh.streben[i].index);
            bx, by, bz = getWorldTranslation(self.hh.streben[i].ref);
            self.hh.streben[i].distance = MathUtil.vector3Length(ax-bx, ay-by, az-bz);                
        end;
        
                    
    end;

end;
function hydraulicAnimations:delete()
end;
function hydraulicAnimations:mouseEvent(posX, posY, isDown, isUp, button)
end;
function hydraulicAnimations:keyEvent(unicode, sym, modifier, isDown)
end;

function hydraulicAnimations:onUpdate(dt)
    if self:getIsActive() then
        if self.hh ~= nil then
            -- Rotation des Hydraulikarmes --
            if self.hh.arm ~= nil then
                local rotx, _, _ = getRotation(self.hh.armGetRot);
                rotx = rotx * self.hh.armLengthMulti;
                rotx = rotx + math.rad(self.hh.armAddDeg);
                setRotation(self.hh.arm, rotx, 0, 0);
            end;
            -- Ausrichtung der Streben --
            if self.hh.streben ~= nil then
                for i=1, 2 do
                    local ax, ay, az = getWorldTranslation(self.hh.streben[i].index);
                    local bx, by, bz = getWorldTranslation(self.hh.streben[i].ref);
                    x, y, z = worldDirectionToLocal(getParent(self.hh.streben[i].index), bx-ax, by-ay, bz-az);
                    setDirection(self.hh.streben[i].index, x, y, z, 0, 0, 1);
                    
                    local distance = MathUtil.vector3Length(ax-bx, ay-by, az-bz);
                    local scaleX, scaleY, scaleZ = getScale(self.hh.streben[i].index);
                    local setScaleWert = scaleZ * (distance / self.hh.streben[i].distance);
                    setScale(self.hh.streben[i].index, 1, 1, setScaleWert);
                end;    
            end;
        end;
    end;
end;

function hydraulicAnimations:draw()    
end;
